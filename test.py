#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

import time
from blueduck import rx

__appname__     = "test"
__author__      = "@AUTHOR@"
__copyright__   = ""
__credits__     = ["@AUTHOR@"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""

start = time.time()
rx.write(b'a\n')
assert input() == "a"
end = time.time()
print("elapsed time: {} seconds".format(end - start))
