#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

from getch import getche
from bluepy.btle import Peripheral
from uuid import UUID

__appname__     = "test3"
__author__      = "@AUTHOR@"
__copyright__   = ""
__credits__     = ["@AUTHOR@"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""


# Straight from adafruit libs
NUS_TX_CHAR_UUID = UUID("6E400003-B5A3-F393-E0A9-E50E24DCCA9E")
NUS_RX_CHAR_UUID = UUID("6E400002-B5A3-F393-E0A9-E50E24DCCA9E")


my_addr = "F7:D1:D6:C1:B5:AA"
p = Peripheral(my_addr, "random")


write_char = None

for c in p.getCharacteristics():
    if UUID(str(c.uuid)) == NUS_TX_CHAR_UUID:
        tx = c
    elif UUID(str(c.uuid)) == NUS_RX_CHAR_UUID:
        rx = c

__all__ = {"rx", "tx"}

if __name__ == "__main__":
    print('go')
    while True:
        rx.write(getche().encode())
