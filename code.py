#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
from adafruit_ble.uart import UARTServer

__appname__     = "code"
__author__      = "@AUTHOR@"
__copyright__   = ""
__credits__     = ["@AUTHOR@"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""


kb = KeyboardLayoutUS(Keyboard())

bt_server = UARTServer()

while True:
    bt_server.start_advertising()
    while not bt_server.connected:
        pass

    while bt_server.connected:
        if bt_server.in_waiting:
            kb.write(bt_server.read(1).decode())
